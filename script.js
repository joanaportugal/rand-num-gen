document.getElementById("numberForm").addEventListener("submit", (e) => {
  e.preventDefault();

  const firstNumber = document.getElementById("firstNumber").value;
  const lastNumber = document.getElementById("lastNumber").value;

  if (firstNumber > lastNumber) {
    alert("The smallest number is bigger");
  } else if (firstNumber === lastNumber) {
    alert("Both are equal");
  } else {
    const generatedNumber = Math.random() * lastNumber;
    document.getElementById(
      "numberReveal"
    ).innerHTML = `The generated number is: ${Math.trunc(generatedNumber) + 1}`;
  }
});
